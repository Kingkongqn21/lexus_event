Lexus Event + React Native 
===

This is app backed [React Native](https://facebook.github.io/react-native/) app for the event of lexus.

## Step One : 
   run terminal and "cd" to existing project folder

   run "npm install"

   After install finish run "npm start"

## Step Two : 

    Go to folder ios in folder and open workspace file "lesux.xcworkspace"

    Select the schema of workspace 

![Image](best_flutter_ui_templates/assets/hotel/hotel_booking.png)
![Image](lexus_event/src/resources/Cover.jpg)


