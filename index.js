
import AppNavigators from './src/navigation/rootNavigation'
import { AppRegistry } from 'react-native'
import React from 'react';

const App = () => {
    return (
        <AppNavigators />
    );
}

AppRegistry.registerComponent("lesux", () => App);
