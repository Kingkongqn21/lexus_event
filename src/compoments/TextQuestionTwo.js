import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import config from '../configs/configLink'
import { ConfigQuestion, Colors } from '../configs'

export default class TextQuestionTwo extends Component {
    render() {
        if (config.isUX) {
            return (
                <View>
                    <Text style={styles.textTitle}>
                        {ConfigQuestion.questionOne.two_1.toLocaleUpperCase()}
                    </Text>
                    <Text style={[styles.textCollection, { marginTop: 10 }]}>
                        {" " + ConfigQuestion.questionOne.two_2.toLocaleUpperCase()}
                    </Text>
                </View>
            )
        }
        else if (config.isRX) {
            return (
                <View>
                    <Text style={styles.textTitle}>
                        {ConfigQuestion.questionOne.two_1.toLocaleUpperCase()}
                    </Text>
                    <Text style={[styles.textCollection, { marginTop: 10 }]}>
                        {" " + ConfigQuestion.questionOne.two_2.toLocaleUpperCase()}
                    </Text>
                </View>
            )
        }
        else if (config.isIS) {
            return (
                <Text style={styles.textCollection}>
                    {ConfigQuestion.questionOne.two_1.toLocaleUpperCase()}
                    <Text style={styles.textTitle}>
                        {" " + ConfigQuestion.questionOne.two_2.toLocaleUpperCase()}
                    </Text>
                </Text>
            )
        }
        else if (config.isNX) {
            return (
                <View>
                    <Text style={styles.textTitle}>
                        {ConfigQuestion.questionOne.two_1.toLocaleUpperCase()}
                        <Text style={styles.textCollection}>
                            {" " + ConfigQuestion.questionOne.two_2.toLocaleUpperCase()}
                        </Text>
                    </Text>
                </View>
            )
        }
        else if (config.isES) {
            return (
                <View>
                    <Text style={styles.textTitle}>
                        {ConfigQuestion.questionOne.two_1.toLocaleUpperCase()}
                    </Text>
                    <Text style={[styles.textCollection, { marginTop: 10 }]}>
                        {" " + ConfigQuestion.questionOne.two_2.toLocaleUpperCase()}
                    </Text>
                </View>
            )
        }
        else if (config.isES2) {
            return (
                <Text style={styles.textTitle}>
                    {ConfigQuestion.questionOne.one_1.toLocaleUpperCase()}
                    <Text style={styles.textCollection}>
                        {" " + ConfigQuestion.questionOne.one_2.toLocaleUpperCase()}
                    </Text>
                </Text>
            )
        }
        else {
            return <View />
        }

    }
}


const styles = StyleSheet.create({
    textCollection: {
        color: Colors.white, fontSize: 20, fontFamily: 'NobelBL-Light', textAlign: 'left', maxWidth: 450
    },
    textTitle: {
        color: Colors.white, fontSize: 20, marginHorizontal: 5, fontFamily: "NobelBL-Bold"
    },
})