import React, { memo } from 'react'
import { View, Image, Text, Dimensions, StyleSheet, TouchableOpacity } from 'react-native'
const { width, height } = Dimensions.get('window')
const checkQuestion = memo(({ onCheckTrue, onCheckFalse }) => {
    return (
        <View style={{ marginRight: 20, marginBottom: 100, flexDirection: 'row', height: 200, marginTop: 50, zIndex: 1 ,alignItems:'center'}} >
            <TouchableOpacity onPress={() => onCheckTrue()} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', zIndex: 1 }}>
                <Image style={styles.image} source={require('../resources/true.png')} />
                <Text style={{ color: '#3D8E4F', ...styles.textCheck }}>TRUE</Text>
            </TouchableOpacity>
            <View style={{ width: 0.5, backgroundColor: '#FFF',height:120 }} />
            <TouchableOpacity onPress={() => onCheckFalse()} style={{ flex: 1, alignItems: 'center', justifyContent: 'center', zIndex: 1 }}>
                <Image style={styles.image} source={require('../resources/false.png')} />
                <Text style={{ color: '#B23634', ...styles.textCheck }}>FALSE</Text>
            </TouchableOpacity>
        </View>
    )
})

const styles = StyleSheet.create({
    textCheck: {
        marginTop: 20,
        fontSize: 40
    },
    image: {
        width: 70,
        height: 70
    }
})

export default checkQuestion;