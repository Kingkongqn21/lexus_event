import CheckQuestion from './checkQuestion'
import TextQuestionOne from './TextQuestionOne'
import TextQuestionTwo from './TextQuestionTwo'

export {
    TextQuestionTwo,
    TextQuestionOne,
    CheckQuestion
}