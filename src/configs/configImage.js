import DeviceInfo from 'react-native-device-info';
const bundleId = DeviceInfo.getBundleId();
const linkImage = {
    questionOne: bundleId === "com.demo.lexuss.ux" ? {
        one: require("../resources/UX_1.jpg"),
        two: require("../resources/UX_2.png")
    } : bundleId === "com.34fdsd.4rgrvwdf" ? {
        one: require("../resources/RX_1.png"),
        two: require("../resources/RX_2.png")
    }
            : bundleId === "com.lesssss.is" ? {
                one: require("../resources/IS_1.png"),
                two: require("../resources/IS_2.png")
            }
                : bundleId === 'com.lessssss.nx' ? {
                    one: require("../resources/NX_1.png"),
                    two: require("../resources/NX_2.png")
                }
                    : bundleId === 'com.lesssssss.es' ? {
                        one: require("../resources/ES_1.png"),
                        two: require("../resources/ES_2.png")
                    }
                        : bundleId === 'com.lesssssss.ls' ? {
                            one: require("../resources/ES2_1.png"),
                            two: require("../resources/ES2_2.png")
                        } : {
                                one: require("../resources/UX_1.jpg"),
                                two: require("../resources/UX_2.png")
                            },
}

export default linkImage
