import DeviceInfo from 'react-native-device-info';
const bundleId = DeviceInfo.getBundleId();


const config = {
    isUX: bundleId === "com.demo.lexuss.ux" ? true : false,
    isRX: bundleId === 'com.34fdsd.4rgrvwdf' ? true : false,
    isIS: bundleId === "com.lesssss.is" ? true : false,
    isNX: bundleId === 'com.lessssss.nx' ? true : false,
    isES: bundleId === 'com.lesssssss.es' ? true : false,
    isES2: bundleId === 'com.lesssssss.ls' ? true : false,
}

export default config