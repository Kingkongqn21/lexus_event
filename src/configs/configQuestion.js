import DeviceInfo from 'react-native-device-info';
const bundleId = DeviceInfo.getBundleId();


const question = {
    questionOne: bundleId === "com.demo.lexuss.ux" ? {
        one_1: "THE LEXUS UX’S COMPACT CABIN IS INSPIRED BY ENGAWA. ",
        one_2: "THIS TRADITIONAL JAPANESE ARCHITECTURAL CONCEPT ENCOURAGES A SEAMLESS CONNECTION BETWEEN THE CABIN AND THE EXTERIOR OF THE CAR, GIVING THE DRIVER A BETTER VIEW OF THE ROAD AHEAD.",
        two_1: "LEXUS’S SELF-CHARGING HYBRID TECHNOLOGY LEAVES A LIGHTER FOOTPRINT ON THE ENVIRONMENT AND ON YOUR WALLET.",
        two_2: "WITH SUPERIOR FUEL EFFICIENCY AND REDUCED EMISSIONS, YOU CAN ALSO REDUCE MAINTENANCE AND RUNNING COSTS.",
        code: [1, 3],
        text: "UX"
    } : bundleId === "com.34fdsd.4rgrvwdf" ? {
        one_1: "THE LEXUS RX HAS",
        one_2: "THE WORLD’S FIRST BLADESCAN ADAPTIVE HIGH-BEAM SYSTEM (AHS).",
        one_3: "WHICH CONTROLS MORE PRECISE LIGHT DISTRIBUTION TO BETTER ILLUMINATE THE AREA IN FRONT OF THE CAR AND DRIVERS CAN RECOGNISE PEDESTRIANS MUCH EARLIER, TO ALLOW FOR SAFER DRIVING AT NIGHT.",
        two_1: "THE LEXUS HYBRID DRIVE SYSTEM FEATURES A PETROL ENGINE, ELECTIC MOTOR AND A HYBRID BATTERY.",
        two_2: "WHEN COMBINED, THEY OFFER SMOOTH, POWERFUL PERFORMANCE AND EXHILARATING ACCELERATION, ALONG WITH REDUCED FUEL CONSUMPTION AND EMISSIONS.",
        code: [4, 6],
        text: "RX"
    }
            : bundleId === "com.lesssss.is" ? {
                one_1: "THE LEXUS IS HAS WON THE",
                one_2: "SGCARMART.COM, COMPACT EXECUTIVE SEDAN OF THE YEAR",
                one_3: "FOR 2 CONSECUTIVE YEARS SINCE 2017.",
                two_1: "EVEN OUR WARRANTIES GO THE EXTRA MILE. LEXUS HYBRIDS HAVE A",
                two_2: "10-YEAR HYBRID BATTERY WARRANTY.",
                code: [7, 9],
                text: "IS"
            }
                : bundleId === 'com.lessssss.nx' ? {
                    one_1: "WITH LEXUS SAFETY SYSTEM+ THE LEXUS NX COMES WITH AN INTEGRATED SUITE OF ADVANCED SAFETY SYSTEMS DESIGNED TO HELP IN CERTAIN REAL-WORLD SITUATIONS. ",
                    one_2: "THEY INCLUDE PRE-COLLISION SYSTEM, LANE DEPARTURE ALERT, ADAPTIVE HIGH-BEAM SYSTEM AND DYNAMIC RADAR CRUISE CONTROL.",
                    two_1: "LEXUS’S SELF-CHARGING HYBRID SYSTEMS REDUCE HARMFUL GAS EMISSIONS",
                    two_2: "TO LEVELS THAT ARE SIGNIFICANTLY LOWER THAN MANY INTERNATIONAL STANDARDS.",
                    code: [10, 12],
                    text: "NX"
                }
                    : bundleId === 'com.lesssssss.es' ? {
                        one_1: "THE LEXUS ES FEATURES THE 17-SPEAKER",
                        one_2: "MARK LEVINSON PREMIUM SURROUND SOUND SYSTEM*.",
                        one_3: "ITS WORLD’S-FIRST DESIGN ARCHITECTURE WAS CREATED TO IMMERSE THE DRIVER AND PASSENGERS IN THE MOST POWERFUL, PRECISE AND PURE AUDIO EXPERIENCE. ",
                        one_4: "*SELECTED VARIANTS ONLY",
                        two_1: "LEXUS HYBRIDS THRIVE IN RUSH HOUR CONDITIONS. REGENERATIVE BRAKING TECHNOLOGY USES KINETIC ENERGY FROM BRAKING TO CHARGE THE ENGINE AS YOU DRIVE, WITH NO PLUG-INS REQUIRED.",
                        two_2: "EVEN AS YOU SAVE ON FUEL, THE PETROL AND ELECTRIC ENGINES COMBINE SEAMLESSLY TO GIVE YOU MORE POWER WHEN YOU NEED IT.",
                        code: [13, 15],
                        text: "ES"
                    }
                        : bundleId === 'com.lesssssss.ls' ? {
                            one_1: "THE LEXUS ES FEATURES AN EXQUISITE SHIMAMOKU WOOD TRIM",
                            one_2: "A MULTI - LAYERED WORK OF ART THAT IS CREATED IN 67 STEPS ACROSS 38 DAYS.",
                            two_1: "UNLIKE ELECTRIC CARS OR PLUG-IN HYBRIDS,",
                            two_2: "YOU’LL NEVER HAVE TO CONNECT A LEXUS HYBRID ELECTRIC VEHICLE TO A POWER SOCKET OR CHARGE OVERNIGHT",
                            two_3: "— THEY’RE SELF-CHARGING AND ALWAYS READY TO GO.",
                            code: [16, 18],
                            text: "ES"
                        } : {
                                one_1: "The Lexus  UX's compact dimensions are inspired by Shodou, a traditional Japanese design concept.",
                                one_2: "",
                                two_1: "Lexus' self-charging hybrid technology leaves a lighter footprint on the environment and on your wallet.",
                                two_2: "With superior fuel efficiency and reduced emissions, you can also reduce maintenance and running costs.",
                                code: [16, 18]
                            },
}

export default question
