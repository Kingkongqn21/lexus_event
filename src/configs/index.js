import Colors from './colors';
import ConfigImage from './configImage';
import ConfigQuestion from './configQuestion';
import ConfigWeb from './configWebPage';
export {
    ConfigQuestion,
    ConfigWeb,
    ConfigImage,
    Colors
}