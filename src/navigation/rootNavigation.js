import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import { IntroScreen, QuestionOne, QuestionTwo, PageCode, PageWeb } from "../screens"

export default createAppContainer(createStackNavigator(
    {
        PageCode: PageCode,
        PageWeb: PageWeb,
        IntroScreen: IntroScreen,
        QuestionOne: QuestionOne,
        QuestionTwo: QuestionTwo
    },
    {
        initialRouteName: 'IntroScreen',
        headerMode: null,
        mode: "modal",
    }
))


