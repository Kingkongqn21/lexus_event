import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet, Alert, TouchableOpacity } from 'react-native'
const { width, height } = Dimensions.get('window')
import { Colors, ConfigQuestion } from '../configs'
import _ from 'lodash'
export default class introScreen extends Component {

    constructor(props) {
        super(props)
        this.interval = null
    }

    state = {
        countDown: 10,
        codeNumber: 1,
    }

    componentDidMount() {
        this.renderViewCountDown()
        this.randomCode()
    }

    randomCode = () => {
        let number = _.random(ConfigQuestion.questionOne.code[0], ConfigQuestion.questionOne.code[1])
        this.setState({ codeNumber: number })
    }

    componentWillUnmount() {
        clearInterval(this.interval)
    }

    render() {
        return (
            <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.navigate("PageWeb")} style={{ flex: 1, backgroundColor: Colors.mainColor }}>
                <View style={{ flexDirection: 'row', padding: 40, marginTop: 150, alignItems: 'center', paddingHorizontal: 100 }}>
                    <View style={{ marginRight: 50 }}>
                        <Text style={styles.textTitle}>THANK YOU FOR PARTICIPATING!</Text>
                        <Text style={styles.textCollection}>HERE’S YOUR CODE:</Text>
                    </View>
                    {this.renderCode()}
                </View>
                <View style={{ flex: 1, alignItems: 'center', marginTop: 70 }}>
                    <Text style={styles.textTitle}>Remember to collect 6 codes</Text>
                    <Text style={styles.textCollection}>to redeem your complimentary gift.</Text>
                    <View style={{ marginTop: 120 }}>
                        <Image style={{ width: 60, height: 60 }} source={require('../resources/rightArrow.png')} />
                    </View>
                    <Text style={{ color: Colors.white, fontSize: 20, fontFamily: 'NobelBL-Light', marginTop: 40, fontWeight: '400' }}>FIND OUT MORE ABOUT THE LEXUS {ConfigQuestion.questionOne.text}</Text>
                </View>
                {this.renderViewLeftBorder()}
                {this.renderViewBottomBorder()}
                {this.renderViewTopBorder()}
                {this.renderViewRightBorder()}
                {this.renderTextCountDown()}
                <View style={{ position: 'absolute', right: 25, top: 25, backgroundColor: Colors.mainColor, padding: 10, zIndex: 1 }}>
                    <Image style={{ width: 250, height: 50 }} resizeMode={'contain'} source={require('../resources/logo.png')} />
                </View>
            </TouchableOpacity>
        )
    }

    renderCode = () => {
        let numberString = this.state.codeNumber + "";
        let numberOne = "0";
        let numberTwo = ""
        if (numberString.length === 1) {
            numberOne = "0";
            numberTwo = numberString
        }
        else if (numberString.length === 2) {
            numberOne = numberString[0];
            numberTwo = numberString[1]
        }
        return (
            <View style={{ width: 300, heigth: 50, flexDirection: 'row' }}>
                {this.renderCodeChart("L")}
                {this.renderCodeChart("E")}
                {this.renderCodeChart("X")}
                {this.renderCodeChart("U")}
                {this.renderCodeChart("S")}
                {this.renderCodeChart(numberOne)}
                {this.renderCodeChart(numberTwo)}
            </View>
        )
    }

    renderCodeChart = (text) => {
        return (
            <View style={{ flexDirection: 'row', alignItems: 'center', width: 60, borderLeftWidth: 0.5, borderRightWidth: 0.5, borderColor: '#FFF', height: 80, justifyContent: 'center' }}>
                <Text style={{ color: Colors.white, fontSize: 60, marginLeft: 10, marginRight: 10, fontFamily: "NobelBL-Light" }}>{text}</Text>
            </View>
        )
    }

    renderTextCountDown = () => {
        return (
            <Text style={{ color: Colors.white, fontSize: 25, position: 'absolute', bottom: 60, right: 60, }}>{this.state.countDown}</Text>
        )
    }

    renderViewLeftBorder = () => {
        return (
            <View style={{ width: 0.5, height: height - 100, marginVertical: 40, position: 'absolute', left: 40, top: 10, backgroundColor: Colors.white }} />
        )
    }

    renderViewBottomBorder = () => {
        return (
            <View style={{ width: width - 80, height: 0.5, marginVertical: 40, position: 'absolute', bottom: 10, left: 40, backgroundColor: Colors.white }} />
        )
    }

    renderViewTopBorder = () => {
        return (
            <View style={{ width: width - 80, height: 0.5, marginVertical: 40, position: 'absolute', top: 10, left: 40, backgroundColor: Colors.white }} />
        )
    }

    renderViewRightBorder = () => {
        return (
            <View style={{ width: 0.5, height: height - 100, marginVertical: 40, position: 'absolute', right: 40, top: 10, backgroundColor: Colors.white }} />
        )
    }

    renderViewCountDown = () => {
        this.interval = setInterval(() => {
            if (this.state.countDown > 0) {
                this.setState({
                    countDown: this.state.countDown - 1
                })
            }
            else {
                clearInterval(this.interval)
                this.props.navigation.navigate('PageWeb')
            }
        }, 1000);
    }
}

const styles = StyleSheet.create({
    textCollection: {
        color: '#FFF', fontSize: 20, fontFamily: 'NobelBL-Light', textAlign: 'center', maxWidth: 450
    },
    textTitle: {
        color: '#FFF', fontSize: 20, marginHorizontal: 5, fontFamily: "NobelBL-Bold"
    },
})