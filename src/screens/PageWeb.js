import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import { WebView } from 'react-native-webview';
import { NavigationActions, StackActions } from 'react-navigation';
import { ConfigWeb } from '../configs'
export default class introScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <WebView style={{ flex: 1 }} source={{ uri: ConfigWeb.link.link }} />
                <TouchableOpacity onPress={() => this.resetToHome()} style={styles.button}><Text style={styles.textGoBack}>GO BACK TO THE QUIZ</Text></TouchableOpacity>
            </View>
        )
    }

    resetToHome = () => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'IntroScreen' })],
        });
        this.props.navigation.dispatch(resetAction);
    }
}

const styles = StyleSheet.create({
    button: {
        width: Dimensions.get('window').width,
        height: 70,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center'
    },
    textGoBack: {
        fontSize: 20,
        color: "#FFF", fontFamily: "NobelBL-Light",
        fontWeight: "500",
    }
})
