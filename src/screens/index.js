import IntroScreen from './introScreen';
import QuestionOne from './questionOne';
import QuestionTwo from './questionTwo';
import PageCode from './PageCode';
import PageWeb from './PageWeb';

export {
    PageCode,
    PageWeb,
    QuestionOne,
    QuestionTwo,
    IntroScreen
}