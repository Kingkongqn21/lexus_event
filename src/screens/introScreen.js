import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Dimensions, StyleSheet } from 'react-native'
const { width, height } = Dimensions.get('window')
import { Colors } from '../configs'
export default class introScreen extends Component {
    render() {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate("QuestionOne")} activeOpacity={1} style={{ flex: 1, flexDirection: 'row' }}>
                <Image resizeMode={'cover'} source={require('../resources/Cover.jpg')} style={{ width: width / 2, height: height }} />
                <View style={{ flex: 1, backgroundColor: Colors.mainColor, alignItems:'center', paddingTop: 150,paddingRight:30 }}>
                    <Text style={styles.textTitle}>
                        REDEEM EXCLUSIVE
                    </Text>
                    <Text style={styles.textTitle}>
                        LEXUS MERCHANDISE
                    </Text>
                    <Text style={styles.textTitle}>
                        BY COLLECTING 6 CODES
                    </Text>
                    <View style={styles.viewSimpleText}>
                        <View style={{ width: 150, height: 0.2, backgroundColor: Colors.white }} />
                        <Text style={styles.simpleText}>Simply :</Text>
                        <View style={{ width: 150, height: 0.2, backgroundColor: Colors.white }} />
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ alignItems: 'center' }}>
                            <View style={styles.arrowNumber}>
                                <Text style={{ color: Colors.white, fontSize: 30 }}>1</Text>
                            </View>
                            <Text style={styles.viewTextQuestion}>Answer 2 questions on each iPad</Text>
                        </View>
                        <View style={{ alignItems: 'center' }}>
                            <View style={styles.arrowNumber}>
                                <Text style={{ color: Colors.white, fontSize: 30 }}>2</Text>
                            </View>
                            <Text style={styles.viewTextQuestion}>Collect the code that appears at the end</Text>
                        </View>
                    </View>
                    <View style={styles.line} />
                    <Text style={styles.textCollection}>
                        Collect all 6 codes to redeem your
                        </Text>
                    <Text style={styles.textCollection}>
                        complimentary gift from the information counter
                        </Text>
                    <Image style={styles.iconDown} source={require('../resources/down.png')} />
                    <Text style={styles.textTabScreens}>Tap Screen to start.</Text>
                </View>
                <View style={styles.viewBorederAll} />
                <View style={{ position: 'absolute', right: 25, top: 25, backgroundColor: Colors.mainColor, padding: 10 }}>
                    <Image style={{ width: 150, height: 50 }} resizeMode={'contain'} source={require('../resources/logo.png')} />
                </View>
            </TouchableOpacity >
        )
    }
}

const styles = StyleSheet.create({
    textCollection: {
        color: Colors.white, fontSize: 20, fontFamily: 'NobelBL-Light', textAlign: 'center'
    },
    textTitle: {
        color: Colors.white, fontSize: 35, fontFamily: "NobelBL-Light"
    },
    simpleText: {
        color: Colors.white, width: 90, textAlign: 'center', fontSize: 30, fontFamily: "NobelBL-Light",
    },
    viewSimpleText: {
        flexDirection: 'row', alignItems: 'center', marginTop: 20, marginBottom: 20
    },
    line: {
        width: 300, height: 0.5, backgroundColor: Colors.white, marginTop: 20, marginBottom: 10, marginTop: 5
    },
    viewTextQuestion: {
        color: Colors.white, fontSize: 20, marginHorizontal: 20, maxWidth: 200, textAlign: 'center', fontFamily: "NobelBL-Light", marginTop: 10
    },
    iconDown: {
        width: 30, height: 20, marginTop: 30
    },
    textTabScreens: {
        color: Colors.white, fontSize: 20, fontFamily: 'NobelBL-Light'
    },
    arrowNumber: {
        alignItems: 'center', justifyContent: 'center', width: 60, height: 60, borderRadius: 30, borderWidth: 1, borderColor: Colors.white
    },
    viewBorederAll: {
        position: 'absolute',
        width: width - 100,
        height: height - 100,
        margin: 50,
        borderWidth: 0.3, borderColor: Colors.white,
    }
})