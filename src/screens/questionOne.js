import React, { Component } from 'react'
import { Text, View, TouchableOpacity, Image, Dimensions, StyleSheet, Alert } from 'react-native'
import { CheckQuestion, TextQuestionOne } from '../compoments'
const { width, height } = Dimensions.get('window')
import { Colors, ConfigImage, ConfigQuestion } from '../configs'
export default class introScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <Image resizeMode={'cover'} source={ConfigImage.questionOne.one} style={{ width: width / 2, height: height }} />
                <View style={{ flex: 1, backgroundColor: Colors.mainColor, paddingTop: 150, paddingHorizontal: 60 }}>
                    <TextQuestionOne />
                    <CheckQuestion onCheckTrue={() => this.props.navigation.navigate('QuestionTwo')} onCheckFalse={() => this.checkAlertQuestion(1)} />
                </View>
                {this.renderViewLeftBorder()}
                {this.renderViewBottomBorder()}
                {this.renderViewTopBorder()}
                {this.renderViewRightBorder()}
                {this.renderTextButtonPosition()}
                {this.renderTextButtonCollection()}
                <View style={{ position: 'absolute', right: 25, top: 25, backgroundColor: Colors.mainColor, padding: 10, zIndex: 1 }}>
                    <Image style={{ width: 150, height: 50 }} resizeMode={'contain'} source={require('../resources/logo.png')} />
                </View>
            </View>
        )
    }

    renderViewLeftBorder = () => {
        return (
            <View style={{ width: 0.5, height: height - 230, marginTop: 40, marginBottom: 80, position: 'absolute', left: 40, top: 10, backgroundColor: Colors.white }} />
        )
    }

    renderViewBottomBorder = () => {
        return (
            <View style={{ width: width - 120, height: 0.5, marginVertical: 40, position: 'absolute', bottom: 10, left: 80, backgroundColor: Colors.white }} />
        )
    }

    renderViewTopBorder = () => {
        return (
            <View style={{ width: width - 80, height: 0.5, marginVertical: 40, position: 'absolute', top: 10, left: 40, backgroundColor: Colors.white }} />
        )
    }

    renderViewRightBorder = () => {
        return (
            <View style={{ width: 0.5, height: height - 100, marginVertical: 40, position: 'absolute', right: 40, top: 10, backgroundColor: Colors.white }} />
        )
    }

    renderTextButtonPosition = () => {
        return (
            <Text style={{ color: '#333333', position: 'absolute', bottom: 0, right: 40, fontSize: 80, fontFamily: 'NobelBL-Light' }}>{ConfigQuestion.questionOne.text}</Text>
        )
    }

    renderTextButtonCollection = () => {
        return (
            <View style={{ position: 'absolute', alignItems: 'center', bottom: 25, left: 20, backgroundColor: 'transparent' }}>
                <Text style={{ color: 'rgba(255, 255, 255, 0.7)', fontFamily: 'NobelBL-Light', fontSize: 40 }}>01</Text>
                <View style={{ width: 40, height: 1, backgroundColor: 'rgba(255, 255, 255, 0.7)' }} />
                <Text style={{ color: 'rgba(255, 255, 255, 0.7)', fontFamily: 'NobelBL-Light', fontSize: 40 }}>02</Text>
            </View>
        )
    }

    checkAlertQuestion = () => {
        const title = "Please try again";
        Alert.alert(
            title,
            "",
            [
                { text: 'OK', onPress: () => console.log('ss') },
            ],
            { cancelable: false },
        );
    }
}

const styles = StyleSheet.create({
    textCollection: {
        color: Colors.white, fontSize: 20, fontFamily: 'NobelBL-Light', textAlign: 'left', maxWidth: 450, marginTop: 40
    },
    textTitle: {
        color: Colors.white, fontSize: 20, marginHorizontal: 5, fontFamily: "NobelBL-Bold"
    },
    simpleText: {
        color: Colors.white, width: 100, textAlign: 'center', fontSize: 30, fontFamily: "NobelBL-Light",
    },
    viewSimpleText: {
        flexDirection: 'row', alignItems: 'center', marginTop: 20, marginBottom: 40
    },
    line: {
        width: 500, height: 0.5, backgroundColor: Colors.white, marginTop: 20, marginBottom: 10, marginTop: 5
    },
    viewTextQuestion: {
        color: Colors.white, fontSize: 25, marginHorizontal: 20, maxWidth: 250, textAlign: 'center', fontFamily: "NobelBL-Light", marginTop: 10
    },
    iconDown: {
        width: 30, height: 20, marginTop: 60
    },
    textTabScreens: {
        color: Colors.white, fontSize: 25, fontFamily: 'NobelBL-Light'
    },
    arrowNumber: {
        alignItems: 'center', justifyContent: 'center', width: 80, height: 80, borderRadius: 40, borderWidth: 1, borderColor: Colors.white
    },
    viewBorederAll: {
        position: 'absolute',
        width: width - 100,
        height: height - 100,
        margin: 50,
        borderWidth: 0.3, borderColor: Colors.white,
    }
})